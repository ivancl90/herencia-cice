package com.inheritance.herencia.globals

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.inheritance.herencia.models.Vehiculo

abstract class GlobalAdapter<T>(private var items : MutableList<T>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    override fun getItemCount(): Int {
        return items?.count() ?: 0
    }

    fun getItem(position: Int) : T? {
        return items?.get(position)
    }

    fun addItems(newItems : List<T>){
        if (items == null)
            items = mutableListOf()

        items?.addAll(newItems)
    }

}